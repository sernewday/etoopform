<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>ET OOP Form</title>
    <meta name="description" content="ET OOP Form">
    <meta name="author" content="Serhii Aksonov">
    <link href="images/favicon.png" rel="shortcut icon" />
    <link rel="stylesheet" href="https://cdn.webix.com/edge/webix.css" type="text/css" />
    <script src="https://cdn.webix.com/edge/webix.js" type="text/javascript"></script>
</head>

<body>
<script type="text/javascript">
var result = true;
var form1 = [
  {template:"Index Form", type:"section"},
  { view:"text", label:'Login(a-Z,0-9,_,а-Я only)', id:"login", name:"login",},
  { view:"text", label:'Password(a-Z,0-9,_,а-Я only and length not less 8)', id:"password", name:"password"},
  { view:"text", label:'Phone(ex.Ukr. +380504455666)', id:"phone", name:"phone"},
  { view:"text", label:'E-mail', id:"email", name:"email"},
  { view:"text", label:'Payment Card (VISA, MC only)', id:"paycard", name:"paycard"},
  { view:"text", label:'Expiration Date(mm/yy)', id:"expdate", name:"expdate"},
  { view:"text", label:'CVC/CVV', id:"cvvcode", name:"cvvcode"},  
  { view:"button", css:"webix_primary",
    value: "Submit",
    click:function(){
    var form = this.getParentView();
    if (form.validate()) {
      webix.message("Data Sent");
      webix.ajax().post('/etcontroller.php', {
        login:$$('login').getValue(),
        password:$$('password').getValue(),
        phone:$$('phone').getValue(),
        email:$$('email').getValue(),
        paycard:$$('paycard').getValue(),
        expdate:$$('expdate').getValue(),
        cvvcode:$$('cvvcode').getValue(), 
        }, function(text, data, XmlHttpRequest) {
            result = data.json();
            if (result) 
              webix.message({type:"info", text:"Data accepted and approved"});
            else
              webix.message({type:"error", text:"Data rejected"});
        });
    }
    else
      webix.message({ type:"error", text:"Same Filds is Empty" });
  }},
{ view:"button", 
    value:"Clear Form",
    click:function(){
        $$('any_form').clear();
}},
];

webix.ui({
  view:"form",
  id:"any_form", 
  scroll:false, 
  width:400,
  css: { margin:"auto"},
  elements: form1,
  rules:{
    $all:webix.rules.isNotEmpty
  },
  elementsConfig:{
    labelPosition:"top"
  }
});	

</script>
</body>

</html>