<?php
abstract class AbstractForm {
    //create form
    abstract public function createForm();
    //get form data
    abstract public function getFormData();          
}