<?php
// its not class, its list types of validation whose been in my plan,
//but not have enough time
abstract class listValidator {
    //text(string) data validation
    abstract public function textValidate();
    //textarea data validation
    abstract public function textareaValidate();
    //number data validation
    abstract public function numberValidate();
    //decimal data validation
    abstract public function decimalValidate();
    //date data validation
    abstract public function dateValidate();
    //datetime data validation 
    abstract public function datetimeValidate();
    //time data validation
    abstract public function timeValidate();
    //day data validation
    abstract public function dayValidate();
    //weak data validation
    abstract public function weakValidate();
    //year data validation
    abstract public function yearValidate();
    //url data validation
    abstract public function urlValidate();
    //file data validation
    abstract public function fileValidate();
    //image data validation
    abstract public function imageValidate();
    //color data validation
    abstract public function colorValidate(); 
    //paymant card number data validation (VISA, MC)
      
}